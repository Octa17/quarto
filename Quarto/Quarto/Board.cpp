#include "Board.h"
//#include <tuple>

Piece& Board::operator[](const Position& p)
{
    uint8_t line=p.first, column=p.second;
   // std::tie(line, column) = p;
   //const  auto& [line, column] = p;
   return m_Board[line * 4 + column];
}

const Piece& Board::operator[](const Position& p) const
{
    uint8_t line = p.first, column = p.second;
    return m_Board[line * 4 + column];
}

std::ostream& operator<<(std::ostream& out, const Board&b)
{
    Board::Position p;
    uint8_t line = p.first, column = p.second;
    for (line = 0; line < 4; line++)
    {
        for (column = 0; column < 4; column++)
        {
            out << b[p];
            out << " ";
        }
        out << std::endl;
    }
    return out;
}
