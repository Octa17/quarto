#include "Piece.h"

Piece::Piece(Colour c, Shape s, Height h, BodyStyle b)
:m_Colour(c), m_Shape(s), m_Height(h), m_BodyStyle(b)
{
}


Piece::Colour  Piece::getColour() 
{
	return m_Colour;
}

Piece::Shape const Piece::getShape()
{
	return m_Shape;
}

Piece:: Height const Piece::getHeight() 
{
	return m_Height;
}

Piece:: BodyStyle const Piece::getBodyStyle() 
{
	return m_BodyStyle;
}

const std::ostream& operator<<(std::ostream& out, const Piece& piece)
{
	return out << int(piece.m_Colour) << " " <<int( piece.m_Shape )<< " " << int(piece.m_Height) << " " << int(piece.m_BodyStyle);
}
