#include <iostream>
#include "Piece.h"
#include "Board.h"
int main()
{
	Piece piece(Piece::Colour::Black, Piece::Height::Short, Piece::Shape::Round, Piece::BodyStyle::Full);
	std::cout << piece;
	Board board;
	std::cout << board;
	return 0;
}