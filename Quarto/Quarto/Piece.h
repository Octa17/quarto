#pragma once
#include <iostream>

class Piece
{
public:
	enum class Colour: uint8_t
	{
		Black,
		White
	};
	enum class Shape: uint8_t
	{
		Round,
		Square
	};
	enum class Height: uint8_t
	{
		Short, 
		Tall
	};
	enum class BodyStyle: uint8_t
	{
		Full,
		Hollow
	};

	Piece(Colour c, Shape s, Height h, BodyStyle b);
	Piece() = default;
	Colour getColour(); const
	Shape getShape(); const
	Height getHeight(); const
	BodyStyle getBodyStyle(); const
	friend std::ostream& operator<<(std::ostream& out, const Piece& piece);
private:
	Colour m_Colour: 1;
	Shape m_Shape: 1;
	Height m_Height: 1;
	BodyStyle m_BodyStyle: 1;
};

